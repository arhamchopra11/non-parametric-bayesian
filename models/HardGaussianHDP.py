import numpy as np
import pandas as pd
#import matplotlib.pyplot as plt
import math
import sys
from sklearn.datasets import fetch_20newsgroups
from random import randint

#newsgroups_train = fetch_20newsgroups(subset='train')

# set following variables and fill data such that
# data is a list of docs with each doc as list of points
#lambda_l and lambda_g are input
lambda_l=100000
lambda_g=12000


dim = 5
nDoc = 10
data = []

for i in range(nDoc):
	dpoint = randint(50,100)
	docTemp = []
	for j in range(dpoint):
		temp = np.random.randint(500, size=dim)
		docTemp.append(temp)
	data.append(docTemp)


totalPts = 0
sumDoc = []
for i in range(nDoc):
	nElem = len(data[i])
	totalPts = totalPts + nElem
	tempSum = [sum(j) for j in zip(*data[i])]
	sumDoc.append(tempSum)

tempSum = [sum(j) for j in zip(*sumDoc)]

#average to total datapoints
avg_data = [x / totalPts for x in tempSum]
	

# Global Variables
g = 1
mu = [avg_data]				#mu is list of global clusters
k = [1 for i in range(nDoc)]	
z = []						#z is local cluster association
for j in range(nDoc):
	temp = [0 for i in range(len(data[j]))]
	z.append(temp)

v = [[] for j in range(nDoc)]
for j in range(nDoc):
	v[j].append(0)

maxiters = 500
iters = 0
while iters<maxiters:
	iters = iters + 1
	print("Iteration no "+str(iters)+" Global clusters = "+str(g))
	#step 4
	print("Step 4")
	for j in range(nDoc):
		nElem = len(data[j])
		for i in range(nElem):
			#point x_ij
			d=[]
			for p in range(g):
				tempDist = np.dot(data[j][i]-mu[p],data[j][i]-mu[p])
				if p not in v[j]:
					tempDist = tempDist + lambda_l
				d.append(tempDist)
			if min(d) > lambda_l+lambda_g:
				z[j][i] = k[j]
				v[j].append(g)
				k[j] = k[j] + 1
				g = g + 1
				mu.append(data[j][i])
			else:
				pbar = d.index(min(d))
				if pbar in v[j]:
					c = v[j].index(pbar)
					z[j][i] = c
					v[j][c] = pbar
				else:
					z[j][i] = k[j]
					v[j].append(pbar)
					k[j] = k[j] + 1
	#step 5

	print("Step 5")
	for j in range(nDoc):
		localC = max(z[j])+1
		S = [[] for i in range(localC)]
		nElem = len(data[j])
		for i in range(nElem):
			S[z[j][i]].append(data[j][i])
		mubar = []
		for c in range(localC):
			ne =  len(S[c])
			tempSum = [sum(t) for t in zip(*S[c])]
			mubar_jc = [x / ne for x in tempSum]
			mubar.append(mubar_jc)
		
			dbar = []
			for p in range(g):
				tempSum = 0
				for s in range(len(S[c])):
					tempSum = tempSum + np.dot(S[c][s]-mu[p],S[c][s]-mu[p])
				dbar.append(tempSum)
			barSum = 0
			for s in range(len(S[c])):
				barSum = barSum + np.dot(S[c][s]-mubar_jc,S[c][s]-mubar_jc)
			if min(dbar)>lambda_g + barSum:
				v[j][c]=g
				mu.append(mubar_jc)
				g = g+1
			else:
				v[j][c]=dbar.index(min(dbar))


	#step 6

	print("Step 6")
	L = [[] for j in range(g)]
	for j in range(nDoc):
		nElem = len(data[j])
		for i in range(nElem):
			L[v[j][z[j][i]]].append(data[j][i])
	#print(len(mu[1]))
	#print(L)
	mu = []
	for p in range(g):
		ne =  len(L[p])
		if ne==0:
			tempSum = [0]*dim
			ne = 1
		else:
			tempSum = [sum(t) for t in zip(*L[p])]
		avg_temp = [x / ne for x in tempSum]
		#print(len(avg_temp))
		mu.append(avg_temp)
	#print(mu)

print(g)