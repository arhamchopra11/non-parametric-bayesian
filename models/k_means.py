import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math
import sys

# Cluster the data
def cluster(data, k, no_epochs):
    #  Read the dimension of the points from a point
    dim = len(data[0, :])
    avg_data = np.average(data, axis=0)

    #  Cluster Assignment for each point
    cluster_assignment = [-1]*len(data)
    #  Randomly assign the initial means of clusters
    means = sigma*np.random.randn(k, dim)+np.repeat([avg_data], k, axis  = 0 )

    for i in range(no_epochs):
        #  Calculate the distance of each point from the means, 
        #  we don't need to calculate the norm of data points 
        #  as they will be same for each mean
        norm_means = np.sum(means*means, axis = 1)
        similarity = np.dot(data, means.T)
        means_norm = np.repeat([norm_means], len(data), axis=0)
        point_dist = -2*similarity + means_norm

        #  Assign each point to cluster
        cluster_assignment = np.argmin(point_dist, axis = 1)

        #  Update the means as per the new cluster assignments
        for cluster_id in range(k):
            new_mean = np.average(data[cluster_assignment==cluster_id, :], axis = 0)
            if not math.isnan(new_mean[0]):
                means[cluster_id] = new_mean
    return (means, cluster_assignment)

def euclidean_loss(means, data, cluster_assignment):
    #  Calculate the acutal distance between means and all the points
    data_norm = np.repeat([np.sum(data*data, axis=1)], len(means), axis=0).T
    means_norm = np.repeat([np.sum(means*means, axis=1)], len(data), axis = 0)
    similarity = np.dot(data, means.T)
    dist = np.sqrt(data_norm - 2*similarity + means_norm)

    #  Total loss
    loss = 0

    #  For each cluster find the loss due to distance from 
    #  the corresponding mean
    for cluster_id in range(len(means)):
        cluster_loss = np.sum(np.min(dist[cluster_assignment==cluster_id, :], axis=1))
        if not math.isnan(cluster_loss):
            loss += cluster_loss
    return loss
    
def plots_cluster(means, data, cluster_assignment):
    cmap = plt.get_cmap('gnuplot')
    colors = [cmap(i) for i in np.linspace(0, 1, len(means))]

    plt.figure()
    for cluster_id in range(len(means)):
        cluster_data = data[cluster_assignment==cluster_id, :]
        plt.plot(cluster_data[:, 0], cluster_data[:, 1], 'x', color=colors[cluster_id])
        plt.plot(means[cluster_id, 0], means[cluster_id, 1], 'o', color=colors[cluster_id])
        
    plt.show()

def plot_losses(k_range, losses, euclidean=1, AIC=1, BIC=1):
    plt.figure()
    if euclidean == 1:
        plt.plot(k_range, losses["euclidean"])
    if AIC == 1:
        plt.plot(k_range, losses["AIC"])
    if BIC == 1:
        plt.plot(k_range, losses["BIC"])
    plt.show()

if __name__ == "__main__":
    #  Handling the inputs
    if len(sys.argv) <2 or len(sys.argv)>5:
        print("Usage python k_means.py datafile start_size=1 end_size=25 no_epochs=50")
        sys.exit()
    elif len(sys.argv) == 5:
        start_size = int(sys.argv[2])
        end_size = int(sys.argv[3])
        no_epochs = int(sys.argv[4])

    elif len(sys.argv) == 4:
        start_size = int(sys.argv[2])
        end_size = int(sys.argv[3])
        no_epochs = 50
    else:
        start_size = 1
        end_size = 25
        no_epochs = 50
    filepath = sys.argv[1]

    # Global Variables
    k_range = [i for i in range(start_size, end_size+1)]
    losses = {
            "euclidean":[],
            #  "AIC":[],
            #  "BIC":[]
            }
    sigma = 100

    # Load dataset
    data = pd.read_csv(filepath, header=None, delim_whitespace=True)
    data = data.values

    for k in k_range:
        print("For k={}".format(k))
        means, cluster_assignment = cluster(data, k, no_epochs)
        
        losses["euclidean"].append(euclidean_loss(means, data, cluster_assignment))
        #  losses["AIC"].append(AIC_loss(data, means))
        #  losses["BIC"].append(BIC_loss(data, means))
        print("The loss is l={}".format(losses["euclidean"]))
        
        plots_cluster(means, data, cluster_assignment)
    plot_losses(k_range, losses, euclidean=1, AIC=0, BIC=0)
