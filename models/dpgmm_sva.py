import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math
import sys
import argparse

def cal_means(clusters):
    cluster_means = []
    for cluster_ind in clusters.keys():
        cluster_means.append(
                np.average(np.array(clusters[cluster_ind]), axis=0)
                )
    return cluster_means

def assign_clusters(data, means):
    norm_means = np.sum(means**2, axis = 1)
    similarity = np.dot(data, means.T)
    means_norm = np.repeat([norm_means], len(data), axis=0)
    point_dist = -2*similarity + means_norm

    cluster_assignment = np.argmin(point_dist, axis = 1)
    return cluster_assignment

def get_clusters(data, cluster_assignment):
    cluster = {}
    for ind, point in enumerate(data):
        if cluster_assignment[ind] not in cluster.keys():
            cluster[cluster_assignment[ind]] = [point]
        else:
            cluster[cluster_assignment[ind]].append(point)
    return cluster

# Cluster the data
def cluster(data, max_sep, no_epochs):
    print("Starting to make clusters")
    #  Read the dimension of the points from a point
    dim = len(data[0, :])
    mean = np.average(data, axis=0)

    #  Initialize the variables
    no_clusters = 1
    cluster_assignments = [0 for point in data]
    cluster_means = [mean]
    clusters = {}
    print("Initialization")
    print("cluster_means:{}".format(cluster_means))
    print("No of clusters. {}".format(no_clusters))
    
    for epoch in range(no_epochs):
        for ind, point in enumerate(data):
            dist =  np.sum((np.array(cluster_means) - \
                np.repeat([point], no_clusters, axis=0))**2, axis = 1)
            min_dist = np.min(dist)
            min_dist_cluster = np.argmin(dist)
            if min_dist > max_sep**2:
                cluster_assignments[ind] = no_clusters
                cluster_means.append(point)
                no_clusters += 1
            else:
                cluster_assignments[ind] = min_dist_cluster

        clusters = get_clusters(data, cluster_assignments)
        cluster_means = cal_means(clusters)
        no_clusters = len(cluster_means)
        #  print("cluster_means:{}".format(cluster_means))
        print("No of clusters:{}".format(no_clusters))
    
    cluster_assignments = assign_clusters(data, np.array(cluster_means))
    print("Clustering done")
    return (np.array(cluster_means), cluster_assignments)

def euclidean_loss(means, data, cluster_assignment):
    #  Calculate the acutal distance between means and all the points
    data_norm = np.repeat([np.sum(data*data, axis=1)], len(means), axis=0).T
    means_norm = np.repeat([np.sum(means*means, axis=1)], len(data), axis = 0)
    similarity = np.dot(data, means.T)
    dist = np.sqrt(data_norm - 2*similarity + means_norm)

    #  Total loss
    loss = 0

    #  For each cluster find the loss due to distance from 
    #  the corresponding mean
    for cluster_id in range(len(means)):
        cluster_loss = np.sum(np.min(dist[cluster_assignment==cluster_id, :], axis=1))
        if not math.isnan(cluster_loss):
            loss += cluster_loss
    return loss
    
def plots_cluster(means, data, cluster_assignment):
    cmap = plt.get_cmap('gnuplot')
    colors = [cmap(i) for i in np.linspace(0, 1, len(means))]

    plt.figure()
    for cluster_id in range(len(means)):
        cluster_data = data[cluster_assignment==cluster_id, :]
        plt.plot(cluster_data[:, 0], cluster_data[:, 1], 'x', color=colors[cluster_id])
        plt.plot(means[cluster_id, 0], means[cluster_id, 1], 'o', color=colors[cluster_id])
        
    plt.show()

def plot_losses(k_range, losses, euclidean=1, AIC=1, BIC=1):
    plt.figure()
    if euclidean == 1:
        plt.plot(k_range, losses["euclidean"])
    if AIC == 1:
        plt.plot(k_range, losses["AIC"])
    if BIC == 1:
        plt.plot(k_range, losses["BIC"])
    plt.show()

def euclidean_loss(means, data, cluster_assignment):
    #  Calculate the acutal distance between means and all the points
    data_norm = np.repeat([np.sum(data*data, axis=1)], len(means), axis=0).T
    means_norm = np.repeat([np.sum(means*means, axis=1)], len(data), axis = 0)
    similarity = np.dot(data, means.T)
    dist = np.sqrt(data_norm - 2*similarity + means_norm)

    #  Total loss
    loss = 0

    #  For each cluster find the loss due to distance from 
    #  the corresponding mean
    for cluster_id in range(len(means)):
        cluster_loss = np.sum(np.min(dist[cluster_assignment==cluster_id, :], axis=1))
        if not math.isnan(cluster_loss):
            loss += cluster_loss
    return loss

if __name__ == "__main__":
    #  Parse Arguments
    parser = argparse.ArgumentParser()

    parser.add_argument('--data', type=str, required=True
            , help='path to datafile')
    parser.add_argument('--start_sep', type=int, 
            default=1, help='Start separation')
    parser.add_argument('--end_sep', type=int, 
            default=5, help='End separation')
    parser.add_argument('--jump_sep', type=int, 
            default=1, help='Jump between separation')
    parser.add_argument('--iters', type=int, 
            default=1, help='No of iterations')

    args = parser.parse_args()
    args_dict = vars(args)

    filepath = args_dict["data"]
    start_sep = args_dict["start_sep"]
    end_sep = args_dict["end_sep"]
    jump_sep = args_dict["jump_sep"]
    iters = args_dict["iters"] 

    # Global Variables
    k_range = [i for i in range(start_sep, end_sep+1, jump_sep)]
    print(k_range)
    losses = {
            "euclidean":[],
            #  "AIC":[],
            #  "BIC":[]
            }

    # Load dataset
    data = pd.read_csv(filepath, header=None, delim_whitespace=True)
    data = data.values

    for k in k_range:
        print("For k={}".format(k))
        means, assignments = cluster(data, k, iters)
        #  plots_cluster(means, data, assignments)

        losses["euclidean"].append(euclidean_loss(means, data, assignments))
        #  losses["AIC"].append(AIC_loss(data, means))
        #  losses["BIC"].append(BIC_loss(data, means))
        print("The loss is l={}".format(losses["euclidean"]))
        
    plot_losses(k_range, losses, euclidean=1, AIC=0, BIC=0)
