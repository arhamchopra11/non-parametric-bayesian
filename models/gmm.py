import numpy as np
import pandas as pd
#import matplotlib.pyplot as plt
import math
import sys
from scipy.stats import multivariate_normal
from collections import namedtuple
class GMM:
    def __init__(self, k, eps = 0.0001):
        self.k = k 
        self.eps = eps 
    
    def fit_EM(self, X, max_iter):
        
        n, d = X.shape
        #print(self.k)
        mu=(data[np.random.choice(n, self.k, False)])
        Sigma = [np.eye(d)*1000000000] * self.k
        
        #print("mu")
        #print(mu)
        #print("sigma")
        #print(Sigma)
                
        w = [1./self.k] * self.k
        
        # responsibility matrix is initialized to all zeros 
        R = np.zeros((n, self.k))
        log_likelihoods = []
        #P = lambda data,mu, s: ((((2 * np.pi)**k) * np.linalg.det(s)) ** -.5 ) * np.exp(-.5 *( np.dot(data - mu * (np.dot(np.linalg.inv(s) * (X - mu).T)).T)))      
        i = 1;
        while len(log_likelihoods) < max_iter:
            print("iter = " + str(i))
            i = i+1
            for k in range(self.k):
                #print(mu[k])
                #print("sigma")
                #print(Sigma)
                #exit()
                var = multivariate_normal(mean=mu[k], cov=Sigma[k])
                #print(sum(var.pdf(X)))
                R[:, k] = w[k] * var.pdf(X)
            
            log_likelihood = np.sum(np.log((np.sum(R, axis = 1))))
            log_likelihoods.append(log_likelihood)
            
            R = np.nan_to_num(R.T / np.sum(R, axis = 1)).T


#           
#
#--------------------------------M-step-----------------------------------
#            
#

            
            N_ks = np.sum(R, axis = 1)
            print("N_ks")
            print(sum(sum(R)))

            for k in range(self.k):
                ## calulate means
                temp = 1. / N_ks[k] * np.sum(R[:, k] * X.T, axis = 1).T
                #print("mu")
                #print(temp)
                x_mu = np.matrix(X - mu[k])
                ## covariances
                Sigma[k] = np.array(np.dot(np.multiply(x_mu.T,  R[:, k]), x_mu))
                #print("Sigma")
                #print(Sigma)

                ## and finally the probabilities
                w[k] = 1. / n * N_ks[k]
            # check for onvergence
            if len(log_likelihoods) < 2 : continue
            if np.abs(log_likelihood - log_likelihoods[-2]) < self.eps: break
        
        
        self.params = namedtuple('params', ['mu', 'Sigma', 'w', 'log_likelihoods', 'num_iters'])
        self.params.mu = mu
        self.params.Sigma = Sigma
        self.params.w = w
        self.params.log_likelihoods = log_likelihoods
        self.params.num_iters = len(log_likelihoods)     
        return self.params

        
if __name__ == "__main__":
    #  Handling the inputs
    if len(sys.argv) <4 or len(sys.argv)>4:
        print("ERROR : usage python3 gmm.py datafile no_clusters max_iter")
        exit()
    else:
        k = int(sys.argv[2])
        max_iter = int(sys.argv[3])

    filepath = sys.argv[1]

    data = pd.read_csv(filepath, header=None, delim_whitespace=True)
    data = data.values
    
    
    eps = 0.0001
    sigma = 100

    gmm = GMM(k, eps)
    params = gmm.fit_EM(data, max_iter)
